<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::post('refresh', 'UserController@refresh');

Route::middleware('jwt.auth')->get('balance', 'UserController@getUserBalance');
Route::middleware('jwt.auth')->post('payment', 'PaymentController@createPayment');
Route::middleware('jwt.auth')->get('payment', 'PaymentController@getUserPayments');
Route::middleware('jwt.auth')->get('payment/{id}', 'PaymentController@getPayment');