<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Psy\Util\Json;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Model\Payment;
use App\Http\Requests\CreatePaymentRequest;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    public function getUserPayments(){
        $userId = User::getUser()->id;
        $payments = Payment::where('user_id',$userId)->get();
        return $payments;
    }

    public function createPayment(Request $request){
        $user = User::getUser();
        $userId = $user->id;
        $userBalance = $user->balance;

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'amount' => 'required',
            'recipient_wallet' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $amount = intval($request->get('amount'));

        if($userBalance < $amount){
            return response()->json('Not enough founds', 409);
        }

        DB::beginTransaction();

        try{
            $user->balance = $user->balance-$amount;
            $user->save();

            $payment = Payment::create([
                'name' => $request->json()->get('name'),
                'user_id' => $userId,
                'comment' => $request->json()->get('comment'),
                'amount' => $request->json()->get('amount'),
                'recipient_wallet' => $request->json()->get('recipient_wallet'),
            ]);

            DB::commit();

            return response()->json(['payment' => $payment, 'balance' => $user->balance], 200);

        }catch(\Exception $e){
            DB::rollback();
            return response()->json('can not be created', 409);
        }
    }

    public function getPayment($id){
        $payment = Payment::where('id',$id)->first();
        return $payment;
    }
}
