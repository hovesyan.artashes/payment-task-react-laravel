<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'comment',
        'date',
        'status',
        'amount',
        'recipient_wallet'
    ];

    const STATUS_COMPLETE = 'complete';
    const STATUS_PENDING = 'panging';
    const STATUS_CANCELED = 'canceled';

    const DEFAULT_PAGINATION = 10;
}
