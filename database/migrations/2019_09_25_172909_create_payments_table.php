<?php

use App\Model\Payment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('comment')->nullable();
            $table->enum('status', [
                Payment::STATUS_COMPLETE,
                Payment::STATUS_PENDING,
                Payment::STATUS_CANCELED,
            ])->default(Payment::STATUS_PENDING);
            $table->decimal('amount', 8, 2);
            $table->string('recipient_wallet');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
